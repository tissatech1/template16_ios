//
//  LoginPage.swift
//  Template4
//
//  Created by TISSA Technology on 3/11/21.
//

import UIKit
import Alamofire

class LoginPage: UIViewController {
    @IBOutlet weak var restaurantLbl: UILabel!
    @IBOutlet weak var restaurantTop: NSLayoutConstraint!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var backlogBtn: UIButton!

    
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    var forgotusername = String()
    var forgotpassword = String()
    var getid = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            backlogBtn.isHidden = false
            backlogBtn.layer.cornerRadius = 8
        }else{
            
            backlogBtn.isHidden = true
        }
        self.navigationController?.setNavigationBarHidden(true, animated: true)

        restaurantTop.constant = self.view.frame.height/6
        
        let gradient = getGradientLayer(bounds: restaurantLbl.bounds)
        restaurantLbl.textColor = gradientColor(bounds: restaurantLbl.bounds, gradientLayer: gradient)
        
        view1.layer.cornerRadius = 8
        view1.layer.shadowColor = UIColor.lightGray.cgColor
        view1.layer.shadowOpacity = 1
        view1.layer.shadowOffset = .zero
        view1.layer.shadowRadius = 8
        
        view2.layer.cornerRadius = 8
        view2.layer.shadowColor = UIColor.lightGray.cgColor
        view2.layer.shadowOpacity = 1
        view2.layer.shadowOffset = .zero
        view2.layer.shadowRadius = 8
        
        button1.layer.cornerRadius = 8
        button1.layer.borderColor = self.randomColor().cgColor
        button1.layer.borderWidth = 0.5
        
        button2.layer.cornerRadius = 10
        button2.layer.borderColor = self.randomColor().cgColor
        button2.layer.borderWidth = 0.5
        button2.layer.shadowColor = UIColor.lightGray.cgColor
        button2.layer.shadowOpacity = 1
        button2.layer.shadowOffset = .zero
        button2.layer.shadowRadius = 8
        
        button3.layer.cornerRadius = 10
        button3.layer.borderColor = self.randomColor().cgColor
        button3.layer.borderWidth = 0.5
        button3.layer.shadowColor = UIColor.lightGray.cgColor
        button3.layer.shadowOpacity = 1
        button3.layer.shadowOffset = .zero
        button3.layer.shadowRadius = 8
        
        
    }
    @IBAction func backlogBtnClicked(_ sender: UIButton) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "HomePage") as! HomePage
        self.navigationController?.pushViewController(home, animated: false)
    }
    
    func randomColor() -> UIColor{
            let red = CGFloat(drand48())
            let green = CGFloat(drand48())
            let blue = CGFloat(drand48())
        return UIColor(red: red, green: green, blue: blue, alpha: 1)
    }

    func getGradientLayer(bounds : CGRect) -> CAGradientLayer{
    let gradient = CAGradientLayer()
    gradient.frame = bounds
    //order of gradient colors
    gradient.colors = [UIColor.red.cgColor,UIColor.blue.cgColor, UIColor.green.cgColor]
    // start and end points
    gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
    gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
    return gradient
    }

    func gradientColor(bounds: CGRect, gradientLayer :CAGradientLayer) -> UIColor? {
    UIGraphicsBeginImageContext(gradientLayer.bounds.size)
      //create UIImage by rendering gradient layer.
    gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
      //get gradient UIcolor from gradient UIImage
    return UIColor(patternImage: image!)
    }
    
    @IBAction func loginBtnClicked(_ sender: UIButton) {
        
        if usernameTF.text == nil || usernameTF.text == "UserName" || usernameTF.text == "" || usernameTF.text == " "{
            self.showSimpleAlert(messagess: "Enter username")
        }else if passwordTF.text == nil || passwordTF.text == "Password" || passwordTF.text == "" || passwordTF.text == " "{
            self.showSimpleAlert(messagess: "Enter password")
        }else{
            
            ERProgressHud.sharedInstance.show(withTitle: "Loading...")

            loginApi()
            
        }
    }
    
    @IBAction func SignupBtnClicked(_ sender: UIButton) {
        
        let sign = self.storyboard?.instantiateViewController(withIdentifier: "SignupPage") as! SignupPage
        self.navigationController?.pushViewController(sign, animated: true)
    }
    
    @IBAction func GuestBtnCLicked(_ sender: UIButton) {
        
        guard let popupVC = storyboard?.instantiateViewController(withIdentifier: "OtpViewPage") as? OtpViewPage else { return }
        popupVC.height = self.view.frame.height / 1.4
        popupVC.topCornerRadius = 20
        popupVC.presentDuration = 0.5
        popupVC.dismissDuration = 0.3
        popupVC.shouldDismissInteractivelty = true
        popupVC.popupDelegate = self
        present(popupVC, animated: true, completion: nil)
    }
    
    
    @IBAction func forgotusernameClicked(_ sender: UIButton) {

        var answer = String()
        
        let ac = UIAlertController(title: "Username  Reset", message: nil, preferredStyle: .alert)
       
        ac.addTextField { (textField) in
            textField.placeholder = "Enter your email"
            textField.textAlignment = .center
        }
        
        let submitAction = UIAlertAction(title: "OK", style: .default) { [self, unowned ac] _ in
                 let textstr = ac.textFields![0]
                answer = textstr.text!
                
                if answer == ""{
                    
                    self.showSimpleAlert(messagess: "Enter email")
                }else{
                    
                    forgotusername = answer
                    
                    ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                    usenamereset()
                    
                }
                
            }
        
        let CANCELAction = UIAlertAction(title: "CANCEL", style: .default) {  _ in
            
        }

            ac.addAction(CANCELAction)
            ac.addAction(submitAction)

            present(ac, animated: true)
        ac.view.tintColor = UIColor.black
        
    }
    
    
    @IBAction func forgotpasswordclicked(_ sender: UIButton) {
        
        var answer = String()
 
        let ac = UIAlertController(title: "Password  Reset", message: nil, preferredStyle: .alert)
       
        ac.addTextField { (textField) in
            textField.placeholder = "Enter your username"
            textField.textAlignment = .center
        }
        
            let submitAction = UIAlertAction(title: "OK", style: .default) { [unowned ac] _ in
                let textstr = ac.textFields![0]
               answer = textstr.text!
               
               if answer == ""{
                   
                   self.showSimpleAlert(messagess: "Enter username")
               }else{
                
                self.forgotpassword = answer
                ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                self.forgotpassApi()
                   
               }
                
                
            }
        
        let CANCELAction = UIAlertAction(title: "CANCEL", style: .default) {  _ in
            
        }

            ac.addAction(CANCELAction)
            ac.addAction(submitAction)

            present(ac, animated: true)
        ac.view.tintColor = UIColor.black
        
    }
    
    
}

extension LoginPage: BottomPopupDelegate {
    
    func bottomPopupViewLoaded() {
        print("bottomPopupViewLoaded")
    }
    
    func bottomPopupWillAppear() {
        print("bottomPopupWillAppear")
    }
    
    func bottomPopupDidAppear() {
        print("bottomPopupDidAppear")
    }
    
    func bottomPopupWillDismiss() {
        print("bottomPopupWillDismiss")
       
        if GlobalClass.OtpSubmitClicked == "yes" {
            
            ERProgressHud.sharedInstance.show(withTitle: "Loading...")
            UserVerifyApi()
            
        }else{
            
            
        }
        
    }
    
    func bottomPopupDidDismiss() {
        print("bottomPopupDidDismiss")
    }
    
    func bottomPopupDismissInteractionPercentChanged(from oldValue: CGFloat, to newValue: CGFloat) {
        print("bottomPopupDismissInteractionPercentChanged fromValue: \(oldValue) to: \(newValue)")
    }
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}

extension LoginPage{
    //MARK: Webservice Call
    func adminlogincheck(){

        let urlString = GlobalClass.DevlopmentApi + "rest-auth/login/v1/"

        AF.request(urlString, method: .post, parameters: ["username":GlobalClass.adminusername, "password":GlobalClass.adminpassword,"restaurant_id":"1"],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                            
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(tok, forKey: "custToken")
                            
                            self.UserVerifyApi()
                          
                           }else{
                            
                            if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()
                               // self.sessionAlert()
                                
                            }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                
                               //  print(dict.value(forKey: "msg") as! String)
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                          
                           }
                           
                           break
                       case .failure(let error):
                        
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        
                        if error.localizedDescription == msg {
                            
                            self.showSimpleAlert(messagess:"No internet connection")
                            
                        }else if error.localizedDescription == msgr ||  error.localizedDescription == msgrs {
                            
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                           print(error)
                       }
       }


    }
    
    func UserVerifyApi() {


        let urlString = GlobalClass.DevlopmentApi+"guest/verify/"

        AF.request(urlString, method: .post, parameters: ["phone_number": GlobalClass.globalguestMobile,"verification_code":GlobalClass.globalverifyCode],encoding: JSONEncoding.default, headers: nil).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 200{
                       
                                 
                                 let dict :NSDictionary = response.value! as! NSDictionary
                                 // print(dict)
                                    let status = dict.value(forKey: "id")
                                 let tok = dict.value(forKey: "token")
                                 
                                 let defaults = UserDefaults.standard
                                 
                                 defaults.set(status, forKey: "custId")
                                 defaults.set(tok, forKey: "custToken")
                                 
                                 defaults.set("loged", forKey: "Usertype")
                                defaults.set("guestuser", forKey: "Userlog")
                                GlobalClass.customertoken = tok as! String
                                 
                                 ERProgressHud.sharedInstance.hide()
   
                              //   self.performSegue(withIdentifier: "location", sender: self)
                                
                                let home = self.storyboard?.instantiateViewController(withIdentifier: "HomePage") as! HomePage
                                self.navigationController?.pushViewController(home, animated: true)
                                

                            }else{
                             
                              if response.response?.statusCode == 403{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                 
                                 self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                             }else if response.response?.statusCode == 401{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 self.showSimpleAlert(messagess: "Username or password is incorrect")
                             }else if response.response?.statusCode == 400{
                                
                                ERProgressHud.sharedInstance.hide()

                                let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                            }else{
                                 
                                 self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                }
                             

                            }
                            
                            break
                        case .failure(let error):
                         ERProgressHud.sharedInstance.hide()
                         print(error.localizedDescription)
                         
                         let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                         
                         
                         let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                         
                         let msgrs = "URLSessionTask failed with error: The request timed out."
                         
                         if error.localizedDescription == msg {
                             
                             self.showSimpleAlert(messagess:"No Internet Detected")
                             
                         }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                             
                             self.showSimpleAlert(messagess:"Slow Internet Detected")
                             
                         }else{
                         
                             self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                         }

                            print(error)
                        }
        }


     }
    
    
    
    func addidrestaurant() {

        let defaults = UserDefaults.standard
        
        let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
            
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                
            ]

        let urlString = GlobalClass.DevlopmentApi+"userrestaurant/"

        AF.request(urlString, method: .post, parameters: ["user":getid ,"restaurant":GlobalClass.restaurantGlobalid],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 201{
                             
                             ERProgressHud.sharedInstance.hide()

                                ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                self.loginApi()
                             
                            }else{
                             
                               if response.response?.statusCode == 403{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                 
                                let msgtxt = dict.value(forKey: "msg") as! String
                                 
                                 if msgtxt == "You do not have access to restaurant." {
                                    
                                     let alert = UIAlertController(title: nil, message: "You do not have access to restaurant.You want to login for this restaurant please click on OK.",         preferredStyle: UIAlertController.Style.alert)

                                     alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default, handler: { _ in
                                         //Cancel Action//
                                     }))
                                     alert.addAction(UIAlertAction(title: "OK",
                                                                   style: UIAlertAction.Style.default,
                                                                   handler: {(_: UIAlertAction!) in
                                                                     //Sign out action
                                                                     ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                                                     self.tokenapi()
                                                                      
                                     }))
                                     self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor.black
                                     
                                     
                                     
                                 }else{
                                 
                                 self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                     
                                 }
                                 
                             }else if response.response?.statusCode == 401{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 self.showSimpleAlert(messagess: "Username or password is incorrect")
                             }else{
                                 
                                 self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                }
                             

                            }
                            
                            break
                        case .failure(let error):
                         ERProgressHud.sharedInstance.hide()
                         print(error.localizedDescription)
                         
                         let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                         
                         
                         let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                         
                         let msgrs = "URLSessionTask failed with error: The request timed out."
                         
                         if error.localizedDescription == msg {

                             self.showSimpleAlert(messagess:"No Internet Detected")

                         }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                             self.showSimpleAlert(messagess:"Slow Internet Detected")

                         }else{
                         
                             self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                            print(error)
                        }
        }


     }
    
    
   
    func Getidapi(){
        
       
        let defaults = UserDefaults.standard
        
        let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
        let urlString = GlobalClass.DevlopmentApi+"/user/?username=\(usernameTF.text!)"
        
           
        print(" categoryurl - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                
            ]
      

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                 
                                    let dict : NSDictionary = response.value! as! NSDictionary
                                    
                                    let firstarr : NSArray = dict["results"]as! NSArray
                                    
                                    let firstdict : NSDictionary = firstarr[0]as! NSDictionary
                                    
                                    let firstid = firstdict.value(forKey: "id")
                                    
                                    print(firstid ?? 00)

                                    self.getid = firstid as! Int
                                    
                                    self.addidrestaurant()
                                    
                                }else{
                                    
                  if response.response?.statusCode == 401{
                                    
                    ERProgressHud.sharedInstance.hide()
                  }
                     if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }
                                   
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {

                            self.showSimpleAlert(messagess:"No internet connection")

                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                            self.showSimpleAlert(messagess:"Slow Internet Detected")

                                }else{
                                
                        self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    

    func tokenapi(){

        let urlString = GlobalClass.DevlopmentApi + "rest-auth/login/v1/"

        AF.request(urlString, method: .post, parameters: ["username":GlobalClass.adminusername, "password":GlobalClass.adminpassword,"restaurant_id":"1"],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                            
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(tok, forKey: "adminToken")
                            
                        //    self.signupApi()
                          
                            self.Getidapi()
                            
                           }else{
                            
                            if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()
                               // self.sessionAlert()
                                
                            }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                
                               //  print(dict.value(forKey: "msg") as! String)
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                          
                           }
                           
                           break
                       case .failure(let error):
                        
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        
                        if error.localizedDescription == msg {
                            
                            self.showSimpleAlert(messagess:"No internet connection")
                            
                        }else if error.localizedDescription == msgr ||  error.localizedDescription == msgrs {
                            
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                           print(error)
                       }
       }


    }
    
    
    
   func loginApi() {


       let urlString = GlobalClass.DevlopmentApi+"rest-auth/login/v1/"

    AF.request(urlString, method: .post, parameters: ["username": usernameTF.text as Any, "password": passwordTF.text as Any,"restaurant_id":GlobalClass.restaurantGlobalid],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                               let status = dict.value(forKey: "id")
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(status, forKey: "custId")
                            defaults.set(tok, forKey: "custToken")
                            
                            defaults.set("loged", forKey: "Usertype")
                            
                            defaults.set("reguser", forKey: "Userlog")
                            GlobalClass.customertoken = tok as! String
                            
                            ERProgressHud.sharedInstance.hide()

                            
                            let rest = self.storyboard?.instantiateViewController(withIdentifier: "HomePage") as! HomePage
                            self.navigationController?.pushViewController(rest, animated: true)
                            
                            
                           }else{
                            
                              if response.response?.statusCode == 403{
                                
                                ERProgressHud.sharedInstance.hide()

                                let dict :NSDictionary = response.value! as! NSDictionary
                                
                               let msgtxt = dict.value(forKey: "msg") as! String
                                
                                if msgtxt == "You do not have access to restaurant." {
                                   
                                    let alert = UIAlertController(title: nil, message: "You do not have access to this restaurant. You want to login for this restaurant please click on OK.",         preferredStyle: UIAlertController.Style.alert)

                                    alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default, handler: { _ in
                                        //Cancel Action//
                                    }))
                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in
                                                                    //Sign out action
                                                                    ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                                                    self.tokenapi()
                                                                     
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor.black
                                    
                                    
                                    
                                }else{
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    
                                }
                                
                            }else if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()

                                self.showSimpleAlert(messagess: "Username or password is incorrect")
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                            

                           }
                           
                           break
                       case .failure(let error):
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        if error.localizedDescription == msg {

                            self.showSimpleAlert(messagess:"No Internet Detected")

                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                            self.showSimpleAlert(messagess:"Slow Internet Detected")

                        }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                       }

                           print(error)
                       }
       }


    }
    
    func usenamereset()  {
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
       // let customerId = defaults.integer(forKey: "custId")
        
      
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    let urlString = GlobalClass.DevlopmentApi + "forgot/user/"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]

        AF.request(urlString, method: .post, parameters: ["email":forgotusername],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                
                                    ERProgressHud.sharedInstance.hide()

                                    let alert = UIAlertController(title:nil , message: "Username reset email has been sent",         preferredStyle: UIAlertController.Style.alert)


                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in



                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor.black
                                    
                                 
                                }else{
                                    
                                     if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else if response.response?.statusCode == 400{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    func forgotpassApi() {
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
       // let customerId = defaults.integer(forKey: "custId")
        
      
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    let urlString = GlobalClass.DevlopmentApi + "rest-auth/password/reset/v1/"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]

        AF.request(urlString, method: .post, parameters: ["username":forgotpassword],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                
                                    ERProgressHud.sharedInstance.hide()

                                    let alert = UIAlertController(title:nil , message: "Password reset email has been sent",         preferredStyle: UIAlertController.Style.alert)


                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in



                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor.black
                                    
                                 
                                }else{
                                    
                                     if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else if response.response?.statusCode == 400{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
}
