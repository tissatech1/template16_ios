//
//  PaymentPage.swift
//  Template4
//
//  Created by TISSA Technology on 3/8/21.
//

import UIKit
import SideMenu
import Alamofire

class PaymentPage: UIViewController {
    @IBOutlet weak var addressTabView: UIView!
    @IBOutlet weak var outer1viewpayment: UIView!
    @IBOutlet weak var outer2payment: UIView!
    @IBOutlet weak var payBtn: UIButton!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var addressshowView: UIView!
    @IBOutlet weak var subtotalLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var addressSh1: UILabel!
    @IBOutlet weak var addressSh2: UILabel!
    @IBOutlet weak var addressSh3: UILabel!
    @IBOutlet weak var cardholdernameTF: UITextField!
    @IBOutlet weak var creditcardnumberTF: UITextField!
    @IBOutlet weak var securityTF: UITextField!
    @IBOutlet weak var cardDTETF: UITextField!
    @IBOutlet weak var updateaddBtn: UILabel!

    var MonthStr = String()
    var YearStr = String()
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    var billingaddressData = NSArray()
    var discoutStr = String()
    var servicefeeStr = String()
    var shippoingfeeStr = String()
    var subtotalStr = String()
    var taxStr = String()
    var totalStr = String()
    var tipStr = String()
    var passbillingdata = NSDictionary()
    var StateDict = NSArray()
    var nameadd = String()
    var addressadd = String()
    var hounseadd = String()
    var cityadd = String()
    var stateadd = String()
    var countryadd = String()
    var sipadd = String()
    var companynameadd = String()
    var shippingmethodStr = String()
    var paymentmethodStr = String()
    var customerfullname = String()
    var orderplaced = String()
    var customerPhoneNumber = String()
    var customerEmailId = String()
    var orderresultCurrency = String()
    var orderresultamount = String()
    var orderresultorderid = Int()
    var orderresultcustomerid = Int()
    var shippingMethod = NSArray()
    
    private var selectedDate: AVDate?
    private let calendar: AVCalendarViewController = AVCalendarViewController.calendar
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadshippingMethod()
        getcustomer()
        
        addressTabView.layer.cornerRadius = 8
        addressTabView.layer.shadowColor = UIColor.lightGray.cgColor
        addressTabView.layer.shadowOpacity = 1
        addressTabView.layer.shadowOffset = .zero
        addressTabView.layer.shadowRadius = 8

        cardView.layer.cornerRadius = 8
        outer1viewpayment.backgroundColor = self.randomColor()
        
        outer1viewpayment.layer.cornerRadius = 8
        outer2payment.layer.cornerRadius = 8
        payBtn.layer.cornerRadius = 20
        
        addressshowView.layer.cornerRadius = 16
        addressshowView.layer.borderWidth = 0.5
        addressshowView.layer.borderColor = UIColor.lightGray.cgColor
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        loadBillingAddress()
        
    }
    
    @IBAction func AddressBtnCliked(_ sender: Any) {
        
        guard let popupVC = storyboard?.instantiateViewController(withIdentifier: "AddressPage") as? AddressPage else { return }
        popupVC.height = self.view.frame.height / 1.3
        popupVC.topCornerRadius = 20
        popupVC.presentDuration = 0.5
        popupVC.dismissDuration = 0.5
        popupVC.shouldDismissInteractivelty = true
        popupVC.popupDelegate = self
        present(popupVC, animated: true, completion: nil)
        
   
    }
    
   
    
    func randomColor() -> UIColor{
            let red = CGFloat(drand48())
            let green = CGFloat(drand48())
            let blue = CGFloat(drand48())
        return UIColor(red: red, green: green, blue: blue, alpha: 0.6)
        }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }

    @IBAction func menuBtnClicked(_ sender: Any) {
        
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
    }
    
    @IBAction func payBtnClicked(_ sender: UIButton) {
        
        if cardholdernameTF.text == "" || cardholdernameTF.text == nil {
            showSimpleAlert(messagess: "Enter card holder name")
        }else if creditcardnumberTF.text == "" || creditcardnumberTF.text == nil {
            showSimpleAlert(messagess: "Enter card number")
        }else if securityTF.text == "" || securityTF.text == nil {
            showSimpleAlert(messagess: "Enter security code")
        }else if MonthStr == "" || MonthStr.isEmpty {
            showSimpleAlert(messagess: "Enter Date")
        }else if YearStr == "" || YearStr.isEmpty {
            showSimpleAlert(messagess: "Enter Date")
        }else{

            orderDetailApi()

        }
        
    }
    
    
    @IBAction func calencerBtnCLicked(_ sender: UIButton) {
        
        showTheCalendar()
        
    }
    
    private func showTheCalendar() {
        calendar.dateStyleComponents = CalendarComponentStyle(backgroundColor: UIColor.white,
                                                              textColor: .black,
                                                              highlightColor: UIColor(red: 126/255, green: 192/255, blue: 196/255, alpha: 1.0).withAlphaComponent(0.5))
        calendar.yearStyleComponents = CalendarComponentStyle(backgroundColor: UIColor.lightGray,
                                                              textColor: .black, highlightColor: .white)
        calendar.monthStyleComponents = CalendarComponentStyle(backgroundColor: UIColor.black,
                                                               textColor: UIColor(red: 126/255, green: 192/255, blue: 196/255, alpha: 1.0),
                                                               highlightColor: UIColor.white)
        calendar.subscriber = { [weak self] (date) in guard let checkedSelf = self else { return }
            if date != nil {
                checkedSelf.selectedDate = date
                let _ = Date(timeIntervalSince1970: TimeInterval(date?.doubleVal ?? 0))
              //  if let day = date?.day, let month = date?.month, let year = date?.year {
                
                if let month = date?.month, let year = date?.year {
                    let dateString = month + "/" + year
                    let datemonth = month
                    let dateyear = year
                    
                    let dateappend = month + "/" + year
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MM/yyyy"
                    let enteredDate = dateFormatter.date(from: dateappend)!
                    let endOfMonth = Calendar.current.date(byAdding: .month, value: 1, to: enteredDate)!
                    let now = Date()
                    if (endOfMonth < now) {
                        
                        self!.showSimpleAlert(messagess: "Expired month")
                        
                        self!.cardDTETF.text = ""
                        self!.cardDTETF.placeholder = "MM/YYYY"
                        self!.MonthStr = ""
                        self!.YearStr = ""
                        
                    } else {
                        // valid
                        print("valid - now: \(now) entered: \(enteredDate)")
                        
                        self!.cardDTETF.text = dateString
                        if datemonth == "Jan" {
                            self!.MonthStr = "01"
                        }else if datemonth == "Feb" {
                            self!.MonthStr = "02"
                        }else if datemonth == "Mar" {
                            self!.MonthStr = "03"
                        }else if datemonth == "Apr" {
                            self!.MonthStr = "04"
                        }else if datemonth == "May" {
                            self!.MonthStr = "05"
                        }else if datemonth == "Jun" {
                            self!.MonthStr = "06"
                        }else if datemonth == "Jul" {
                            self!.MonthStr = "07"
                        }else if datemonth == "Aug" {
                            self!.MonthStr = "08"
                        }else if datemonth == "Sept" {
                            self!.MonthStr = "09"
                        }else if datemonth == "Oct" {
                            self!.MonthStr = "10"
                        }else if datemonth == "Nov" {
                            self!.MonthStr = "11"
                        }else if datemonth == "Dec" {
                            self!.MonthStr = "12"
                        }
                       
                        self!.cardDTETF.text = dateString
                        self!.YearStr = dateyear
                        
                    }
                    
                    
                }
            }
        }
        calendar.preSelectedDate = selectedDate
        self.present(calendar, animated: false, completion: nil)
    }
    
}

extension PaymentPage: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
       // print("SideMenu Appearing! (animated: \(animated))")
        self.view.alpha = 0.5
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Appeared! (animated: \(animated))")
        
        self.view.alpha = 0.5
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappearing! (animated: \(animated))")
        
        self.view.alpha = 1
    }
    
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappeared! (animated: \(animated))")
        
        self.view.alpha = 1
    }
}

extension PaymentPage: BottomPopupDelegate {
    
    func bottomPopupViewLoaded() {
        print("bottomPopupViewLoaded")
    }
    
    func bottomPopupWillAppear() {
        print("bottomPopupWillAppear")
    }
    
    func bottomPopupDidAppear() {
        print("bottomPopupDidAppear")
    }
    
    func bottomPopupWillDismiss() {
        print("bottomPopupWillDismiss")
//        let summary = self.storyboard?.instantiateViewController(withIdentifier: "summaryPage") as! summaryPage
//        self.navigationController?.pushViewController(summary, animated: true)
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        loadBillingAddress()
    }
    
    func bottomPopupDidDismiss() {
        print("bottomPopupDidDismiss")
    }
    
    func bottomPopupDismissInteractionPercentChanged(from oldValue: CGFloat, to newValue: CGFloat) {
        print("bottomPopupDismissInteractionPercentChanged fromValue: \(oldValue) to: \(newValue)")
    }
}

// MARK: - AlertController
extension PaymentPage {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                   //     ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}

extension PaymentPage {
    
    func loadBillingAddress(){
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
        let urlString = GlobalClass.DevlopmentApi + "billing/?customer_id=\(customerId)"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "billing"
            ]

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                 
                                  
                                    
                                 let dict1 :NSDictionary = response.value! as! NSDictionary
                                    
                                    print("billing address - \(dict1)")
                                  
                                self.billingaddressData = (dict1.value(forKey:"results")as! NSArray)
                                    
                                    
                                    
                                    if self.billingaddressData.count == 0 {
                                        
                                    cardholdernameTF.isUserInteractionEnabled = false
                                    creditcardnumberTF.isUserInteractionEnabled = false
                                    securityTF.isUserInteractionEnabled = false
                                    cardDTETF.isUserInteractionEnabled = false
                                        
                                        payBtn.isHidden = true
                                        addressshowView.alpha = 0.3
                                        cardView.alpha = 0.3
                                        updateaddBtn.text = "Add Address"
                                        let pulse1 = CASpringAnimation(keyPath: "transform.scale")
                                        pulse1.duration = 0.6
                                        pulse1.fromValue = 1.0
                                        pulse1.toValue = 0.9
                                        pulse1.autoreverses = true
                                        pulse1.repeatCount = 1
                                        pulse1.initialVelocity = 0.5
                                        pulse1.damping = 0.8

                                        let animationGroup = CAAnimationGroup()
                                        animationGroup.duration = 2.7
                                        animationGroup.repeatCount = 1000
                                        animationGroup.animations = [pulse1]

                                        addressTabView.layer.add(animationGroup, forKey: "pulse")
                                        
                                        let firstobj:NSDictionary  = self.billingaddressData.object(at: 0) as! NSDictionary
                                        let defaults = UserDefaults.standard
                                        defaults.set(firstobj, forKey: "billingaddressDICT")
                                        
                                        self.passbillingdata = firstobj
                                        
                                        self.nameadd = firstobj["name"]as! String
                                        self.addressadd = firstobj["address"]as! String
                                        self.hounseadd = firstobj["house_number"]as! String
                                        self.cityadd = firstobj["city"]as! String
                                        self.stateadd = firstobj["state"]as! String
                                        self.countryadd = firstobj["country"]as! String
                                        self.sipadd = firstobj["zip"]as! String
                                        
                                        addressSh1.text = ""
                                        addressSh2.text = ""
                                        addressSh3.text = ""
                                        
                                ERProgressHud.sharedInstance.hide()

 
                               }else{
                                
                                addressTabView.layer.removeAllAnimations()
                                cardholdernameTF.isUserInteractionEnabled = true
                                creditcardnumberTF.isUserInteractionEnabled = true
                                securityTF.isUserInteractionEnabled = true
                                cardDTETF.isUserInteractionEnabled = true
                                
                               fisrttimefeeapi()
                                  let firstobj:NSDictionary  = self.billingaddressData.object(at: 0) as! NSDictionary
                                  
                                let defaults = UserDefaults.standard
                                defaults.set(firstobj, forKey: "billingaddressDICT")
                                
                                payBtn.isHidden = false
                                addressshowView.alpha = 1
                                cardView.alpha = 1
                                updateaddBtn.text = "Update Address"
                              
                                self.passbillingdata = firstobj
                                
                                self.nameadd = firstobj["name"] as! String
                                self.addressadd = firstobj["address"] as! String
                                self.hounseadd = firstobj["house_number"] as! String
                                self.cityadd = firstobj["city"] as! String
                                self.stateadd = firstobj["state"] as! String
                                self.countryadd = firstobj["country"] as! String
                                self.sipadd = firstobj["zip"] as! String
                                
                                addressSh1.text = self.nameadd
                                addressSh2.text = self.addressadd + "," + hounseadd
                                addressSh3.text = cityadd + "," + stateadd + "," + countryadd + "," + sipadd
                                
                                ERProgressHud.sharedInstance.hide()

                               }
                                     
                                 
                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()

                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 404{
                                        
                                       
                                        
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                        
                                    }
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                                    self.showSimpleAlert(messagess:"Slow Internet Detected")
                                            
                                        }else{
                                        
                                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                        }

                                           print(error)
                                    }
            }
            
      
            
        }
   
    func fisrttimefeeapi() {
        
         let defaults = UserDefaults.standard
     
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)

        var subtotalStrfee = String()
        var notaxtotalstr = String()
     
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"

        let cartarray:NSArray = defaults.object(forKey: "cartarray")as! NSArray

        print("cart array - \(cartarray)")
        
       // var sumnotax = 0.00
        var sumsub = 0.00
           
        notaxtotalstr = "0.00"
        subtotalStrfee = "0.00"
        
        let passedcartprice = defaults.object(forKey: "totalcartPrice")as? String
        
        subtotalStrfee = passedcartprice!
    
        print("customerId - \(customerId)")
    //    print("getshippingId - \(getshippingId)")
        print("carttotal - \(subtotalStrfee)")
        
        print("no_tax_total - \(notaxtotalstr)")
        print("sub_total - \(subtotalStrfee)")
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "fee"
            ]

               let urlString = GlobalClass.DevlopmentApi+"fee/"

        AF.request(urlString, method: .post, parameters: ["sub_total": subtotalStrfee, "no_tax_total": notaxtotalstr,"customer_id": customerId,"tip": "0","restaurant":GlobalClass.restaurantGlobalid],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
               response in
                 switch response.result {
                               case .success:
                                print(response)

                                   if response.response?.statusCode == 200{
                                    
                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                     print("firstTimefee Result - \(dict)")
                                       
   
                                        self.discountLbl.text = "$\((dict["discount"] as! String))"
                                        discoutStr = (dict["discount"] as! String)
                             
                                        servicefeeStr = (dict["service_fee"] as! String)

                                        shippoingfeeStr = (dict["shipping_fee"] as! String)


                            self.subtotalLbl.text = "$\((dict["sub_total"] as! String))"
                                        subtotalStr = (dict["sub_total"] as! String)

                                        
                            self.taxLbl.text = "$\((dict["tax"] as! String))"
                                        taxStr = (dict["tax"] as! String)

                                        
                            self.totalLbl.text = "$\((dict["total"] as! String))"
                                        totalStr = (dict["total"] as! String)

                                       
                                    ERProgressHud.sharedInstance.hide()

                                    
                                   }else{
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    if response.response?.statusCode == 401{
                                        
                                        ERProgressHud.sharedInstance.hide()
                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                
                                    
                                   }
                                   
                                   break
                               case .failure(let error):
                                
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                               }
               }


            }
    
    func getcustomer()  {
        
        let defaults = UserDefaults.standard
        
        let savedUserData = defaults.object(forKey: "custToken")as? String
        
        let customerid = defaults.integer(forKey: "custId")
        let custidStr = String(customerid)
        
        let token = "Token \(savedUserData ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
            
        let urlString = GlobalClass.DevlopmentApi+"customer/?customer_id="+custidStr+""
        print("Url cust avl - \(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": token
            ]

             AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                
                                print(response)

                                if response.response?.statusCode == 200{
                                    ERProgressHud.sharedInstance.hide()

                                    let dict :NSDictionary = response.value! as! NSDictionary
                                                //   print(dict)
                                    
                                    let status = dict.value(forKey: "results")as! NSArray
                                                 //  print(status)

                                        print("customer detail - \(status)")

                                    let firstobj:NSDictionary  = status.object(at: 0) as! NSDictionary
                                    
                                    self.customerPhoneNumber = firstobj["phone_number"] as! String
                                    
                                    print(self.customerPhoneNumber)
                                    
                                    let customerinfo:NSDictionary = firstobj.value(forKey: "customer")as! NSDictionary
                                    
                                    
                                    self.customerEmailId = customerinfo["email"] as! String
                                    
                                //    self.customerEmailId = ""
                                    
                                    print(self.customerEmailId)
                                    
                               
                                    let firstnamesh = customerinfo["first_name"] as! String
                                    let lastnamesh = customerinfo["last_name"] as! String

                                    
                                    self.customerfullname = firstnamesh + " " + lastnamesh
                                    
                               
                                    
                                    
                      }else{
                                    
                                //    self.dissmiss()
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        
                                    }else if response.response?.statusCode == 401{
                                        ERProgressHud.sharedInstance.hide()

                                    self.SessionAlert()
                                        
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                            
                                print(response)
                                }
                                
                                break
                            case .failure(let error):
                                
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    func orderDetailApi() {


       let discountdata = discoutStr

        let servicefeedata = servicefeeStr

      //  let servicefeedata = "0.00"

        let shippingfeedata = shippoingfeeStr

        let subtotaldata = subtotalStr

        let taxdata = taxStr

        let totaldata =  totalStr

        let tipdata = "0"
        
        let commentStr = ""
        
          let  cuurency = "USD"
        

       // let cuurency = globelObjectVC.countryfixglob
        print("get currency - \(cuurency)")
        var customrtid = String()

         let defaults = UserDefaults.standard

     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)

        customrtid = String(customerId)

        let getshippingId = defaults.object(forKey: "clickedShippingMethodId")as! Int
       let carttotal = defaults.object(forKey: "totalcartPrice")as! String

        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"


        print("customerId - \(customerId)")
        print("getshippingId - \(getshippingId)")
        print("carttotal - \(carttotal)")

        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)

            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "order-detail"
            ]

               let urlString = GlobalClass.DevlopmentApi+"order-detail/"

        AF.request(urlString, method: .post, parameters: ["status": "active", "currency": cuurency,"subtotal": subtotaldata,"total": totaldata,"extra": commentStr, "customer": customrtid,"tip": tipdata,"service_fee": servicefeedata,"tax": taxdata,"discount": discountdata,"shipping_fee": shippingfeedata,"cart_id": cartidStr],encoding: JSONEncoding.default, headers: headers).responseJSON {
               response in
                 switch response.result {
                               case .success:
                                 //  print(response)

                                   if response.response?.statusCode == 200{

                                    self.orderplaced = "yes"
                                    
                                    let dict :NSDictionary = response.value! as! NSDictionary

                                     print("order detail Result - \(dict)")

                                    let defaults = UserDefaults.standard
                                    defaults.set(dict, forKey: "passOrderResult")

                                    self.orderresultCurrency = (dict["currency"] as! String)
                                    self.orderresultamount = (dict["total"] as! String)
                                    self.orderresultorderid = (dict["order_id"] as! Int)
                                    self.orderresultcustomerid = (dict["customer"] as! Int)


                                   
                                   // self.show()
                                    self.paymentApi()
                                    
                                   
                                    

                                   }else{

                                    ERProgressHud.sharedInstance.hide()

                                    if response.response?.statusCode == 401{

                                        ERProgressHud.sharedInstance.hide()
                                        self.SessionAlert()

                                    }else if response.response?.statusCode == 500{

                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary

                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }



                                   }

                                   break
                               case .failure(let error):
                                
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                               }
               }


            }

    func paymentApi()  {
        
        
        let defaults = UserDefaults.standard
       
    //   let admintoken = defaults.object(forKey: "adminToken")as? String
       let admintoken = defaults.object(forKey: "custToken")as? String
       let customerId = defaults.integer(forKey: "custId")
        let  customeridStr = String(customerId)

        
       let getshippingId = defaults.object(forKey: "clickedShippingMethodId")as! Int
        let shippingmethodName = defaults.object(forKey: "clickedShippingMethod")as! String
        
        print(shippingmethodName)
        let shippingmethodID = defaults.integer(forKey: "clickedShippingMethodId")
        
       let storepassid = (defaults.object(forKey: "clickedStoreId")as? String)!
        
        print(storepassid)
       
        let commentStr = ""
        
       print("commentStr - \(commentStr)")
      
       let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"

      
        
        var metadataDict = [String : Any]()
        
        metadataDict = ["order_id":orderresultorderid, "restaurant_id":storepassid, "customer_id":orderresultcustomerid, "shippingmethod_id":shippingmethodID,"phone":customerPhoneNumber,"name":customerfullname,"special_instruction":commentStr]
        
        print(metadataDict)
        
        
        
        var cardDataDict = [String : Any]()
        
        cardDataDict = ["number":creditcardnumberTF.text!, "exp_month":MonthStr,"exp_year":YearStr, "cvc":securityTF.text!]
        
        print(cardDataDict)
        
        
        let billinfo:NSDictionary = defaults.object(forKey: "billingaddressDICT")as! NSDictionary

        print("billinfo dict - \(billinfo)")
        
        let cityStr = billinfo["city"] as! String
        
        let address2 = "\(billinfo["house_number"] as! String)" + "\(billinfo["address"] as! String)"
        
        let address1 = billinfo["company_name"] as! String
        
        let postalStr = billinfo["zip"] as! String
        
        let stateStr = billinfo["state"] as! String
        
        let addressDict = ["city":cityStr, "line1":address1,"line2":address2, "postal_code":postalStr,"state":stateStr]
        
        
        var addressDataDict = [String : Any]()
        
        addressDataDict = ["address":addressDict]
        print(addressDataDict)
   
       print("customerId - \(customerId)")
       print("getshippingId - \(getshippingId)")
        print("addresspassed - \(addressDataDict)")
       
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let orderidStrheader = String(orderresultorderid)
           
           let headers: HTTPHeaders = [
               "Content-Type": "application/json",
               "Authorization": autho,
               "order_id": orderidStrheader,
               "user_id": customeridStr,
               "cart_id": cartidStr,
               "action": "payment"
           ]

              let urlString = GlobalClass.DevlopmentApi+"payment/"

       AF.request(urlString, method: .post, parameters: ["currency": "USD", "amount": orderresultamount,"receipt_email": customerEmailId,"type": "card","card": cardDataDict,"billing_details": addressDataDict,"metadata": metadataDict],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
              response in
                switch response.result {
                              case .success:
                                 // print(response)

                                  if response.response?.statusCode == 200{
                                   
                                   let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    print(dict)
                                    
//                                    let shippingMDict:NSDictionary = dict["shippingmethod"]as! NSDictionary
//
//                                    shippingmethodStr = shippingMDict["name"]as! String
                           //         paymentmethodStr = dict["payment_method"]as! String
                                  
                                    print("payment Resultshow - \(dict)")
                                    ERProgressHud.sharedInstance.hide()

                                    let alert = UIAlertController(title:nil , message: "Order placed successfully",         preferredStyle: UIAlertController.Style.alert)

                                  
                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in
                                                                    
                                                                    let home = self.storyboard?.instantiateViewController(withIdentifier: "SummaryPage") as! SummaryPage
                                                                    
                                                home.orderIDGet = self.orderresultorderid
                                                                    
                                                                    self.navigationController?.pushViewController(home, animated: true)
                                        
                                                                    
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor.black
                                   
                                    
                                   
                                  }else{
                                   
                                    ERProgressHud.sharedInstance.hide()

                                    if response.response?.statusCode == 400{
                                        
                                        ERProgressHud.sharedInstance.hide()
                                        self.showSimpleAlert(messagess: "Incorrect card details. Please check")
                                        
                                    }else if response.response?.statusCode == 401{
                                       
                                        ERProgressHud.sharedInstance.hide()
                                    self.SessionAlert()
                                       
                                   }else if response.response?.statusCode == 500{
                                       
                                    ERProgressHud.sharedInstance.hide()

                                       let dict :NSDictionary = response.value! as! NSDictionary
                                       
                                       self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                   }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                   }
                                   
                               
                                   
                                  }
                                  
                                  break
                              case .failure(let error):
                                
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                              }
              }


           }
    
    func loadshippingMethod(){
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let storeId = defaults.object(forKey: "clickedStoreId")as? String
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
        let urlString = GlobalClass.DevlopmentApi + "shipping-method/?status=ACTIVE&restaurant=\(storeId ?? "115")"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]

             AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                 
                                  
                                    
                                 let dict1 :NSDictionary = response.value! as! NSDictionary
                                    
                                    print("shipping Method - \(dict1)")
                                  
                                self.shippingMethod = (dict1.value(forKey:"results")as! NSArray)
                                    
                                    print("shipping Method result - \(self.shippingMethod)")
                                    
                                      if self.shippingMethod.count == 0 {
                                       
                                        ERProgressHud.sharedInstance.hide()

//                                        self.viewhide()
//                                        self.shippingmethodfullview.isHidden = true
//                                        self.storeselectLbl.isHidden = true
//                                        self.homeselctLbl.isHidden = true
                                        
                                        self.showSimpleAlert(messagess: "No shipping method available")
                                        
                                     
                                 }else{
                                       
                                    let dictObj = self.shippingMethod[0] as! NSDictionary
                                       
                                    let status = dictObj["name"]
                                    let statusid = dictObj["id"]
                                    
                                    defaults.set(status, forKey: "clickedShippingMethod")
                                    defaults.set(statusid, forKey: "clickedShippingMethodId")
                        
                                    ERProgressHud.sharedInstance.hide()

                                 }
                                     
                                 
                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()

                                        self.SessionAlert()
                                        
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
}
