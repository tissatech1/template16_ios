//
//  OrderCell.swift
//  Template4
//
//  Created by TISSA Technology on 3/10/21.
//

import UIKit

class OrderCell: UICollectionViewCell {
    @IBOutlet weak var outerview: UIView!
    @IBOutlet weak var ordenumberView: UIView!
    @IBOutlet weak var viewdetailsLbl: UILabel!
    @IBOutlet weak var deleteBtnSh: UIButton!
    @IBOutlet weak var orderNoLbl: UILabel!
    @IBOutlet weak var orderDateLbl: UILabel!
    @IBOutlet weak var ordertotalLbl: UILabel!
    @IBOutlet weak var orderStatusLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
