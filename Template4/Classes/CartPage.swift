//
//  CartPage.swift
//  Template4
//
//  Created by TISSA Technology on 3/8/21.
//

import UIKit
import SideMenu
import Alamofire
import SDWebImage

class CartPage: UIViewController {
    @IBOutlet weak var cartTable: UITableView!
    @IBOutlet weak var checkoutBtn: UIButton!
    @IBOutlet weak var carttotalprice: UILabel!

    var CartProducts = NSArray()
    var totatpro = NSString()
    var listproductId = String()
    var totalListSum  = String()
    var getsequenceId  = String()
    var cartitemIdStr  = String()
    var quantityInt  = Int()
    var cartIdStr  = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cartTable.register(UINib(nibName: "CartCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        checkoutBtn.layer.cornerRadius = 20
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        GetCartItems()
    }
    

    func randomColor() -> UIColor{
            let red = CGFloat(drand48())
            let green = CGFloat(drand48())
            let blue = CGFloat(drand48())
        return UIColor(red: red, green: green, blue: blue, alpha: 1)
        }
   
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func menuBtnClicked(_ sender: Any) {
        
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
    }
    
    @IBAction func CheckOutBtnClicked(_ sender: UIButton) {
    
        print("Total Amt - \(totalListSum)")

        let amount = Double(totalListSum);

        if amount == nil {

            self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")

        }else{

        if amount!  == 0 {

            showSimpleAlert(messagess: "No items available in the cart to place order")

        }else{

        let defaults = UserDefaults.standard

        defaults.set(totalListSum, forKey: "totalcartPrice")

        defaults.set(CartProducts, forKey: "cartarray")


            let payment = self.storyboard?.instantiateViewController(withIdentifier: "PaymentPage") as! PaymentPage
            self.navigationController?.pushViewController(payment, animated: true)
                
            
       }

        }

        
        
    }

}


// MARK: - uiTableViewDatasource
extension CartPage: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return CartProducts.count
         

    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CartCell
        cell.selectionStyle = .none
        
        cell.outerSection.layer.cornerRadius = 8
        cell.outerSection.layer.shadowColor = UIColor.lightGray.cgColor
        cell.outerSection.layer.shadowOpacity = 1
        cell.outerSection.layer.shadowOffset = .zero
        cell.outerSection.layer.shadowRadius = 8

        cell.qtySection.layer.cornerRadius = 8
        cell.qtySection.layer.borderColor = self.randomColor().cgColor
        cell.qtySection.layer.borderWidth = 2
        
        cell.imageSection.layer.borderWidth = 1
        cell.imageSection.layer.masksToBounds = false
        cell.imageSection.layer.borderColor = UIColor.lightGray.cgColor
        cell.imageSection.layer.cornerRadius = cell.imageSection.frame.height/2
        cell.imageSection.clipsToBounds = true
        
        cell.deletecartitem.tag = indexPath.row
        cell.deletecartitem.addTarget(self, action: #selector(DeleteBtnPredded(_:)), for: .touchUpInside)
        
        cell.minusBTN.tag = indexPath.row
        cell.minusBTN.addTarget(self, action: #selector(QuantityminusClicked(_:)), for: .touchUpInside)
        
        cell.plusBTN.tag = indexPath.row
        cell.plusBTN.addTarget(self, action: #selector(QuantityplusClicked(_:)), for: .touchUpInside)
        
        let dictObj = self.CartProducts[indexPath.row] as! NSDictionary

       cell.dishnameSec.text = dictObj["product_name"] as? String

       let qnt = dictObj["quantity"] as! Int
       cell.qtyLbl.text = String(qnt)

           let pricedata = dictObj["line_total"]as! String

          cell.pricesection.text = pricedata
        
        var urlStr = String()

        if dictObj["product_url"] is NSNull {
            urlStr = ""
        }else{

            urlStr = dictObj["product_url"] as! String

        }

        let url = URL(string: urlStr )

       cell.imageSection.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imageSection.sd_setImage(with: url) { (image, error, cache, urls) in
            if (error != nil) {
                // Failed to load image
                cell.imageSection.image = UIImage(named: "noimage.png")
            } else {
                // Successful in loading image
                cell.imageSection.image = image
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
            return  107
        
    }
    
    @IBAction func DeleteBtnPredded(_ sender: UIButton) {
           
            let index = sender.tag
           
           let dictObj = self.CartProducts[index] as! NSDictionary
           
           let delpro = dictObj["product_id"] as! Int
           let delseq = dictObj["sequence_id"] as! Int
           
           self.listproductId = String(delpro)
           self.getsequenceId = String(delseq)
           
           showSimpleAlert1()
        
    }
    
    func showSimpleAlert1() {
           let alert = UIAlertController(title: nil, message: "Are you sure you want to delete?",         preferredStyle: UIAlertController.Style.alert)

           alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.default, handler: { _ in
               //Cancel Action//
           }))
           alert.addAction(UIAlertAction(title: "YES",
                                         style: UIAlertAction.Style.default,
                                         handler: {(_: UIAlertAction!) in
                                           
                                            ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                            self.DeleteCartItems()
                                            
           }))
           self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
       }
    
    
    @IBAction func QuantityminusClicked(_ sender: UIButton) {

        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = cartTable.cellForRow(at: indexPath) as! CartCell
        
        let getqty = cell.qtyLbl.text!
        let convertqty = Int(getqty)
        
        if convertqty! > 1 {
         
        quantityInt  = convertqty! - 1
    
         let index = sender.tag
        
        let dictObj = self.CartProducts[index] as! NSDictionary
        
        let delpro = dictObj["product_id"] as! Int
        let delseq = dictObj["sequence_id"] as! Int
        let cartiddStr = dictObj["cart_id"] as! Int
        let cartitemiddStr = dictObj["cart_item_id"] as! Int
        
        self.listproductId = String(delpro)
        self.getsequenceId = String(delseq)
        self.cartIdStr = String(cartiddStr)
        self.cartitemIdStr = String(cartitemiddStr)
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        updateToCart()
            
        }else{
            
            
        }
    }
    
    @IBAction func QuantityplusClicked(_ sender: UIButton) {

        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = cartTable.cellForRow(at: indexPath) as! CartCell
        
        let getqty = cell.qtyLbl.text!
        let convertqty = Int(getqty)
        quantityInt  = convertqty! + 1
       
         let index = sender.tag
        
        let dictObj = self.CartProducts[index] as! NSDictionary
        
        let delpro = dictObj["product_id"] as! Int
        let delseq = dictObj["sequence_id"] as! Int
        let cartiddStr = dictObj["cart_id"] as! Int
        let cartitemiddStr = dictObj["cart_item_id"] as! Int
        
        self.listproductId = String(delpro)
        self.getsequenceId = String(delseq)
        self.cartIdStr = String(cartiddStr)
        self.cartitemIdStr = String(cartitemiddStr)
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        updateToCart()
        
    }
    
    
}

extension CartPage: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
       // print("SideMenu Appearing! (animated: \(animated))")
        self.view.alpha = 0.5
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Appeared! (animated: \(animated))")
        
        self.view.alpha = 0.5
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappearing! (animated: \(animated))")
        
        self.view.alpha = 1
    }
    
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappeared! (animated: \(animated))")
        
        self.view.alpha = 1
    }
}

// MARK: - Api LIst
extension CartPage {
    
    //MARK: Webservice Call Search product by category
        
        
        func GetCartItems(){
            
            let defaults = UserDefaults.standard
            
          //  let admintoken = defaults.object(forKey: "adminToken")as? String
            
            let admintoken = defaults.object(forKey: "custToken")as? String
            
            let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
            
            let cartidStr = String(avlCartId)
            
            let customerid = defaults.integer(forKey: "custId")

            let customeridStr = String(customerid)
            
            let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
          //  let trimmedString = categoryStr.removingAllWhitespaces()
            
            let urlString = GlobalClass.DevlopmentApi+"cart-item/?cart_id=\(avlCartId)&status=ACTIVE&restaurant_id=\(GlobalClass.restaurantGlobalid)"
            
           

                
            print("cartproducts get url - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho,
                    "user_id": customeridStr,
                    "cart_id": cartidStr,
                    "action": "cart-item"
                ]

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                    print(response)

                                    if response.response?.statusCode == 200{
                                     
                                     let dict :NSDictionary = response.value! as! NSDictionary
                                     
                                        
                                           let list:NSArray = dict.value(forKey: "results") as! NSArray
                                        
                                        if list.count == 0 {
                                            
                                            ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                           self.CartProducts = []
                                            self.cartTable.reloadData()

                                            let rupee = "$"
                                            let sum = "0.00"
                                                
                                    let title = rupee + sum
                                     
                                            self.carttotalprice.text = title
                                            
                                
                                            self.totalListSum = "0"
                                            self.showSimpleAlert(messagess:"No items avaible in cart")
                                        }else{
                                        
                                            self.CartProducts = dict.value(forKey: "results")as! NSArray
                                            
                                       let ttl = dict.value(forKey: "total_cost") as! String
                                            
                                            self.totalListSum = ttl
                                            
                                            let rupee = "$"
                                            let title = rupee + ttl
                                            
                                            self.carttotalprice.text = title

                                            
                                             print("Total Amt - \(ttl)")
                                             print("cart products list - \(self.CartProducts)")
                                            
                                            self.cartTable.reloadData()
    
                                        }
                                          
                                        ERProgressHud.sharedInstance.hide()

                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    
                                    if error.localizedDescription == msg {
                                        
                                        self.showSimpleAlert(messagess:"No internet connection")
                                        
                                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                        
                                        self.showSimpleAlert(messagess:"Slow Internet Detected")
                                                
                                            }else{
                                            
                                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                            }

                                               print(error)
                                        }
                }
                
          
                
            }
        

    
    
    //MARK: Webservice Call delete Cart
    
    func DeleteCartItems(){
        
        let defaults = UserDefaults.standard
        
       // let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)

        let customerid = defaults.integer(forKey: "custId")

        let customeridStr = String(customerid)
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
      //  let trimmedString = categoryStr.removingAllWhitespaces()
        
        let urlString = GlobalClass.DevlopmentApi+"cart-item/\(avlCartId)/?product_id=\(listproductId)&sequence_id=\(getsequenceId)"
        
       

            
        print("cartproducts get url - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "cart-item"
            ]

        AF.request(urlString, method: .delete, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                    
                                    
                                    
                                    self.GetCartItems()
                                  print("success")
                                    
                                    
                                }else{
                                    
                                     if response.response?.statusCode == 401{
                                 
                                        ERProgressHud.sharedInstance.hide()
                                        self.SessionAlert()
                                        
                                    }
                                    
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                                    self.showSimpleAlert(messagess:"Slow Internet Detected")
                                            
                                        }else{
                                        
                                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                        }

                                           print(error)
                                    }
            }
            
      
            
        }
    
    //MARK: Webservice Call for update to cart

     
    func updateToCart() {
       
        let defaults = UserDefaults.standard
        
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        if defaults.object(forKey: "AvlbCartId") == nil {
          
            self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
           
            
        }else{
        
         let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
            let cartidStr = String(avlCartId)
            
       // let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let admintoken = defaults.object(forKey: "custToken")as? String
            
            print("customer token -\(String(describing: admintoken))")
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"

        let urlString = GlobalClass.DevlopmentApi+"cart-item/\(cartitemIdStr)/"
        
 
          
            let metadataDict = ["id":cartitemIdStr,"product_id":listproductId, "quantity":quantityInt,"ingredient_id":0,"sequence_id":getsequenceId,"cart":cartIdStr] as [String : Any]
          
            let productarr = [metadataDict]
            print("product element Dict - \(productarr)")
            
          
            
        
            let fileUrl = URL(string: urlString)

            var request = URLRequest(url: fileUrl!)
            request.httpMethod = "PUT"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(autho, forHTTPHeaderField: "Authorization")
            request.setValue(customeridStr, forHTTPHeaderField: "user_id")
            request.setValue(cartidStr, forHTTPHeaderField: "cart_id")
            request.setValue("cart-item", forHTTPHeaderField: "action")


            request.httpBody = try! JSONSerialization.data(withJSONObject: metadataDict)

            AF.request(request)
                .responseJSON { response in
                    // do whatever you want here
                    switch response.result {
                                            case .success:
                                                print(response)
                    
                                                if response.response?.statusCode == 200{
                    
                                                    self.GetCartItems()
                    
                                                }else{
                    
                                                    if response.response?.statusCode == 401{
                    
                                                        ERProgressHud.sharedInstance.hide()
                                                        self.SessionAlert()
                    
                                                    }else if response.response?.statusCode == 500{
                    
                                                        ERProgressHud.sharedInstance.hide()
                    
                                                        let dict :NSDictionary = response.value! as! NSDictionary
                    
                                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                    }else{
                    
                                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                       }
                    
                                                }
                    
                                                break
                                            case .failure(let error):
                                                ERProgressHud.sharedInstance.hide()
                    
                                                print(error.localizedDescription)
                    
                                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                    
                                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                    
                                                let msgrs = "URLSessionTask failed with error: The request timed out."
                    
                                                if error.localizedDescription == msg {
                    
                                            self.showSimpleAlert(messagess:"No internet connection")
                    
                                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                    
                                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                    
                                                }else{
                    
                                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                                }
                    
                                                   print(error)
                                            }
            }
            
  
    }

     }
    
    
}


// MARK: - AlertController
extension CartPage {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                   //     ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}
