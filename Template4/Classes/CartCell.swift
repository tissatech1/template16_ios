//
//  CartCell.swift
//  Template4
//
//  Created by TISSA Technology on 3/8/21.
//

import UIKit

class CartCell: UITableViewCell {
    @IBOutlet weak var imageSection: UIImageView!
    @IBOutlet weak var dishnameSec: UILabel!
    @IBOutlet weak var qtySection: UIView!
    @IBOutlet weak var pricesection: UILabel!
    @IBOutlet weak var qtyLbl: UILabel!
    @IBOutlet weak var plusBTN: UIButton!
    @IBOutlet weak var minusBTN: UIButton!
    @IBOutlet weak var outerSection: UIView!
    @IBOutlet weak var deletecartitem: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
