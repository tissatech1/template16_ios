//
//  ProfilePage.swift
//  Template4
//
//  Created by TISSA Technology on 3/12/21.
//

import UIKit
import SideMenu
import Alamofire
import SDWebImage

class ProfilePage: UIViewController {
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    
    @IBOutlet weak var mmTagTf: UITextField!
    @IBOutlet weak var usernameTf: UITextField!
    @IBOutlet weak var passwordTf: UITextField!
    @IBOutlet weak var mobileTF: UITextField!
    @IBOutlet weak var emailTf: UITextField!
    @IBOutlet weak var codeLbl: UITextField!
    
    var useranmesave = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        saveButton.layer.cornerRadius = 20
        view1.layer.cornerRadius = 8
        view1.layer.shadowColor = UIColor.lightGray.cgColor
        view1.layer.shadowOpacity = 1
        view1.layer.shadowOffset = .zero
        view1.layer.shadowRadius = 8
        
        view2.layer.cornerRadius = 8
        view2.layer.shadowColor = UIColor.lightGray.cgColor
        view2.layer.shadowOpacity = 1
        view2.layer.shadowOffset = .zero
        view2.layer.shadowRadius = 8
        
        view3.layer.cornerRadius = 8
        view3.layer.shadowColor = UIColor.lightGray.cgColor
        view3.layer.shadowOpacity = 1
        view3.layer.shadowOffset = .zero
        view3.layer.shadowRadius = 8
        
        view4.layer.cornerRadius = 8
        view4.layer.shadowColor = UIColor.lightGray.cgColor
        view4.layer.shadowOpacity = 1
        view4.layer.shadowOffset = .zero
        view4.layer.shadowRadius = 8
        
        view5.layer.cornerRadius = 8
        view5.layer.shadowColor = UIColor.lightGray.cgColor
        view5.layer.shadowOpacity = 1
        view5.layer.shadowOffset = .zero
        view5.layer.shadowRadius = 8
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        getprofileData()
        
    }
    
    func randomColor() -> UIColor{
            let red = CGFloat(drand48())
            let green = CGFloat(drand48())
            let blue = CGFloat(drand48())
        return UIColor(red: red, green: green, blue: blue, alpha: 0.5)
    }
    
    @IBAction func menuBtnClicked(_ sender: Any) {
        
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
    }
   
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }

    @IBAction func changepasswordClicked(_ sender: UIButton) {
        
        let change = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordPage") as! ChangePasswordPage
        change.usernameget = useranmesave
        self.navigationController?.pushViewController(change, animated: true)
    }
    
    @IBAction func profilesubmit(_ sender: UIButton) {
           
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        updateprofileApi()

        
       }

}

extension ProfilePage: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
       // print("SideMenu Appearing! (animated: \(animated))")
        self.view.alpha = 0.5
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Appeared! (animated: \(animated))")
        
        self.view.alpha = 0.5
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappearing! (animated: \(animated))")
        
        self.view.alpha = 1
    }
    
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappeared! (animated: \(animated))")
        
        self.view.alpha = 1
    }
}


// MARK: - Api LIst
extension ProfilePage {

    func getprofileData()  {
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    let urlString = GlobalClass.DevlopmentApi + "customer/?customer_id=\(customerId)"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "action": "customer"
            ]

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                 
                                  
                                    
                                 let dict1 :NSDictionary = response.value! as! NSDictionary
                                    
                                  //  print("profile data - \(dict1)")
                                  
                                    let profileAddData:NSArray = (dict1.value(forKey:"results")as! NSArray)
                                    
                                    if profileAddData.count == 0 {
                                     
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                        
                                    }else{
                                    
                                    
                                    let firstobj:NSDictionary  = profileAddData.object(at: 0) as! NSDictionary
                                    
                                    let customerdict:NSDictionary  = firstobj["customer"] as! NSDictionary
                                   
                                    print("profile data - \(firstobj)")
                                    
                                    mmTagTf.text! = firstobj["salutation"]as! String
                                    usernameTf.text! = customerdict["first_name"]as! String
                                    emailTf.text! = customerdict["email"]as! String
                                    passwordTf.text! = customerdict["last_name"]as! String
                                    useranmesave = customerdict["username"]as! String
                                    let number = firstobj["phone_number"]as! String
                                
                                    if number.contains("+91") {
                                        
                                        let result1 = String(number.dropFirst(3))
                                        
                                        mobileTF.text = result1
                                        codeLbl.text = "+91"

                                        
                                    }else{
                                        
                                        let result1 = String(number.dropFirst(2))
                                        
                                        mobileTF.text = result1
                                        codeLbl.text = "+1"
                                    }
                                    
                                    }
                                    ERProgressHud.sharedInstance.hide()

                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()

                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                           }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    
    func updateprofileApi() {
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let numbercomb = codeLbl.text! + mobileTF.text!
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    let urlString = GlobalClass.DevlopmentApi + "user/\(customerId)/"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "action": "user"
            ]

        AF.request(urlString, method: .put, parameters: ["first_name": usernameTf.text!, "last_name":passwordTf.text!,"username":useranmesave,"email":emailTf.text!,"phone_number":numbercomb],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                 
                                 let dict1 :NSDictionary = response.value! as! NSDictionary
                                    
                                    print("profile data - \(dict1)")
                                  
                                    self.updatenumberApi()
                                  
                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()

                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                           }else if response.response?.statusCode == 400{
                            
                            ERProgressHud.sharedInstance.hide()

                            let dict :NSDictionary = response.value! as! NSDictionary
                            
                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
               }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    func updatenumberApi() {
        
        let now = Date()

            let formatter = DateFormatter()

            formatter.timeZone = TimeZone.current

            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS" ///2020-10-19 12:05:10.625

            let dateString = formatter.string(from: now)
        
        print(dateString)
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let numbercomb = codeLbl.text! + mobileTF.text!
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    let urlString = GlobalClass.DevlopmentApi + "customer/\(customerId)/"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "action": "customer"
            ]

        AF.request(urlString, method: .put, parameters: ["first_name": usernameTf.text!, "last_name":passwordTf.text!,"username":useranmesave,"email":emailTf.text!,"phone_number":numbercomb,"last_access":dateString,"salutation":mmTagTf.text!],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                 
                                 let dict1 :NSDictionary = response.value! as! NSDictionary
                                    
                                    print("profile data - \(dict1)")
                                  
//                                    let profileAddData:NSArray = (dict1.value(forKey:"results")as! NSArray)
//
//                                    let firstobj:NSDictionary  = profileAddData.object(at: 0) as! NSDictionary
//
//                                    let customerdict:NSDictionary  = firstobj["customer"] as! NSDictionary
//
//                                    print("profile data - \(firstobj)")
//
//                                    mmTagTf.text! = firstobj["salutation"]as! String
//                                    usernameTf.text! = customerdict["first_name"]as! String
//                                    emailTf.text! = customerdict["email"]as! String
//                                    passwordTf.text! = customerdict["last_name"]as! String
//                                    useranmesave = customerdict["username"]as! String
//                                    let number = firstobj["phone_number"]as! String
//
//                                    if number.contains("+91") {
//
//                                        let result1 = String(number.dropFirst(3))
//
//                                        mobileTF.text = result1
//                                        codeLbl.text = "+91"
//
//
//                                    }else{
//
//                                        let result1 = String(number.dropFirst(2))
//
//                                        mobileTF.text = result1
//                                        codeLbl.text = "+1"
//                                    }
//
                                  
                                    showSimpleAlert(messagess: "Profile updated successfully")
                                    
                                    ERProgressHud.sharedInstance.hide()

                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()

                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                           }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
}


// MARK: - AlertController
extension ProfilePage {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                   //     ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}



