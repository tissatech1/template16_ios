//
//  DetailingredientCell.swift
//  Template4
//
//  Created by TISSA Technology on 3/5/21.
//

import UIKit

class DetailingredientCell: UICollectionViewCell {

    @IBOutlet weak var dishimage: UIImageView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var outerview: UIView!
    @IBOutlet weak var dishname1: UILabel!
    @IBOutlet weak var dishprice: UILabel!
    @IBOutlet weak var selectedimage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
